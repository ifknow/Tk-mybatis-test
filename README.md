# Tk-mybatis-test

#### 介绍
测试tk-mybatis 的增删改查操作

#### 软件架构
本demo采用springboot搭建 mysql5.7


#### 安装教程

1、将项目导入到本地
2、将application.properties中的数据库连接信息改成自己的
3、将项目下的category.sql文件导入到自己的数据库中
4、接下来就可以运行TestApplication启动类
5、可以在postman中对controller接口进行测试
  

#### 使用说明
                        
使用Tk-mybatis对单表数据进行增删改查操作



#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
