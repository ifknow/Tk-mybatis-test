package com.github.orm.tk.mapper;

import com.github.orm.tk.TestApplication;
import com.github.orm.tk.model.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @description: SimpleTkMapperTest <br>
 * @date: 2019/8/30 19:35 <br>
 * @author: Cheri <br>
 * @version: 1.0 <br>
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestApplication.class)
public class SimpleTkMapperTest {

    @Autowired
    CategoryDao categoryDao;

    @Test
    public void test(){
        Category category = new Category();
        category.setCategoryID(1);
        category.setCategoryName("Beverages");
        System.out.println(categoryDao.select(category));

    }


    @Test
    public void selectAllTest() {
        List<Category> categories = categoryDao.selectAll();
        System.out.println(categories);
        assertEquals(true, categories.size() > 0);
    }

    @Test
    public void insertTest() {
        Category newCategory = new Category();
        newCategory.setCategoryID(1000);
        newCategory.setCategoryName("test");
        newCategory.setDescription("for test");
        int result = categoryDao.insert(newCategory);
        assertEquals(1, result);
    }

}
