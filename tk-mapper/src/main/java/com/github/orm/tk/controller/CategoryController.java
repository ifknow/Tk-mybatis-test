package com.github.orm.tk.controller;


import com.github.orm.tk.model.Category;
import com.github.orm.tk.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Author: gongshiyong
 * Date: 2020/6/6  9:00
 * Description: 分类Controller
 */
@RestController
@RequestMapping("/tk/demo")
@Slf4j
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    /**
     * 插入数据
     *
     * @param category
     */
    @PutMapping("/insert")
    public void insert(@RequestBody Category category) {
        categoryService.insert(category);

    }

    /**
     * 根据条件插入数据
     *
     * @param category
     */
    @PutMapping("/insertSelective")
    public void insertSelective(@RequestBody Category category) {
        categoryService.insertSelective(category);
    }

    /**
     * 删除数据
     *
     * @param category
     */
    @DeleteMapping("delete")
    public void delete(@RequestBody Category category) {
        categoryService.delete(category);
    }

    /**
     * 按照条件删除数据
     *
     * @param category
     */
    @DeleteMapping("deleteByExample")
    public void deleteByExample(@RequestBody Category category) {
        categoryService.deleteByExample(category);
    }

    /**
     * 根据主键删除数据
     *
     * @param category
     */
    @DeleteMapping("deleteByPrimaryKey")
    public void deleteByPrimaryKey(@RequestBody Category category) {
        categoryService.deleteByPrimaryKey(category);
    }

    /**
     * 根据条件更新数据
     *
     * @param category
     */
    @PutMapping("updateByExampleSelective")
    public void updateByExampleSelective(@RequestBody Category category) {
        categoryService.updateByExampleSelective(category);
    }

    /**
     * 查询数据
     *
     * @param category
     * @return
     */
    @GetMapping("select")
    public List<Category> select(@RequestBody Category category) {
        List<Category> categories = categoryService.select(category);
        return categories;
    }

    /**
     * 查询所有数据
     *
     * @return
     */
    @GetMapping("selectAll")
    public List<Category> selectAll() {
        List<Category> categories = categoryService.selectAll();
        return categories;
    }

    /**
     * 根据条件查询数据
     *
     * @param category
     * @return
     */
    @GetMapping("selectByExample")
    public List<Category> selectByExample(@RequestBody Category category) {
        List<Category> categories = categoryService.selectByExample(category);
        return categories;
    }


}
