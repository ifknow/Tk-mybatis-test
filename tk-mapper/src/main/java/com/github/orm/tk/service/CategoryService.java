package com.github.orm.tk.service;

import com.github.orm.tk.model.Category;

import java.util.List;

public interface CategoryService {

    /**
     * 向分类表中插入数据
     *
     * @param category
     */
    void insert(Category category);

    /**
     * 按照条件进行插入数据
     *
     * @param category
     */
    void insertSelective(Category category);

    /**
     * 删除数据
     *
     * @param category
     */
    void delete(Category category);

    /**
     * 按照条件进行删除数据
     *
     * @param category
     */
    void deleteByExample(Category category);

    /**
     * 按照主键进行删除数据
     *
     * @param category
     */
    void deleteByPrimaryKey(Category category);

    /**
     * 更新数据
     *
     * @param category
     */
    void updateByExampleSelective(Category category);

    /**
     * 查询所有的数据
     *
     * @return
     */
    List<Category> select(Category category);

    List<Category> selectAll();

    /**
     * 按照条件进行查询
     *
     * @param category
     * @return
     */
    List<Category> selectByExample(Category category);
}
