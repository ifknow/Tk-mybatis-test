package com.github.orm.tk.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @description: Category实体 <br>
 * @date: 2019/8/30 19:31 <br>
 * @author: Cheri <br>
 * @version: 1.0 <br>
 */
@Data
@Table(name = "category")
public class Category {
    @Id
    @Column(name = "category_id")
    private Integer categoryID;
    private String categoryName;
    private String description;
}
