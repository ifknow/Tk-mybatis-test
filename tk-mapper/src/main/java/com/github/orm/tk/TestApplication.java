package com.github.orm.tk;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: TestApplication <br>
 * @date: 2019/8/30 19:36 <br>
 * @author: Cheri <br>
 * @version: 1.0 <br>
 */
@SpringBootApplication
@MapperScan(basePackages = "com.github.orm.tk.mapper")
public class TestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class,args);
    }

}
