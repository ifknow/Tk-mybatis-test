package com.github.orm.tk.service.impl;

import com.github.orm.tk.mapper.CategoryDao;
import com.github.orm.tk.model.Category;
import com.github.orm.tk.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * Author: gongshiyong
 * Date: 2020/6/6  9:05
 * Description: NO Description
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryDao categoryDao;

    @Override
    public void insert(Category category) {
        categoryDao.insert(category);
    }

    @Override
    public void insertSelective(Category category) {
        categoryDao.insertSelective(category);
    }

    @Override
    public void delete(Category category) {
        categoryDao.delete(category);
    }

    @Override
    public void deleteByExample(Category category) {
        Example example = new Example(Category.class);
        example.createCriteria().andEqualTo("categoryID", category.getCategoryID());
        categoryDao.deleteByExample(example);
    }

    @Override
    public void deleteByPrimaryKey(Category category) {
        categoryDao.deleteByPrimaryKey(categoryDao);
    }

    @Override
    public void updateByExampleSelective(Category category) {
        Example example = new Example(Category.class);
        example.createCriteria().andEqualTo("categoryID", category.getCategoryID());
        categoryDao.updateByExampleSelective(category, example);
    }

    @Override
    public List<Category> select(Category category) {
        List<Category> categories = categoryDao.select(category);
        return categories;
    }

    @Override
    public List<Category> selectAll() {
        List<Category> categories = categoryDao.selectAll();
        return categories;
    }

    @Override
    public List<Category> selectByExample(Category category) {
        Example example = new Example(Category.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("categoryID",category.getCategoryID());
        List<Category> categories = categoryDao.selectByExample(example);

        int i = categoryDao.selectCount(category);
        return categories;
    }


}
