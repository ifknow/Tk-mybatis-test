package com.github.orm.tk.mapper;

import com.github.orm.tk.model.Category;
import tk.mybatis.mapper.common.Mapper;

/**
 * @description: CategoryDao <br>
 * @date: 2019/8/30 19:30 <br>
 * @author: Cheri <br>
 * @version: 1.0 <br>
 */
public interface CategoryDao extends Mapper<Category> {
}