/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50710
Source Host           : localhost:3306
Source Database       : orm-tech

Target Server Type    : MYSQL
Target Server Version : 50710
File Encoding         : 65001

Date: 2020-06-06 11:18:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('4', '水果', '水果');
INSERT INTO `category` VALUES ('5', '家电', '家电');
INSERT INTO `category` VALUES ('6', '手机', '手机');
INSERT INTO `category` VALUES ('7', '水杯', 'xxxx');
INSERT INTO `category` VALUES ('8', '水果', '水果');
